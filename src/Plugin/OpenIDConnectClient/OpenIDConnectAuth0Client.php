<?php

namespace Drupal\openid_connect_auth0\Plugin\OpenIDConnectClient;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\openid_connect\Plugin\OpenIDConnectClient\OpenIDConnectGenericClient;

/**
 * Auth0 OpenID Connect client.
 *
 * @OpenIDConnectClient(
 *   id = "auth0",
 *   label = @Translation("Auth0")
 * )
 */
class OpenIDConnectAuth0Client extends OpenIDConnectGenericClient {

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $config = \Drupal::config('auth0.settings');

    $form['form_title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Form title'),
      '#default_value' => $this->configuration['form_title'] ?? $this->t('Sign In'),
      '#description' => $this->t('This is the title for the login widget.'),
    ];

    $form['claim_to_use_for_role'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Claim for Role Mapping:'),
      '#default_value' => $this->configuration['claim_to_use_for_role'] ?? '',
      '#description' => $this->t('Name of the claim to use to map to Drupal roles, e.g. roles.  If the claim contains a list of values, all values will be used in the mappings below.'),
    ];

    $role_mapping = $this->configuration['role_mapping'] ?? [];
    $description = <<<EOD
Enter role mappings here in the format &lt;Auth0 claim value>|&lt;Drupal role name> (one per line), e.g.:</br>
admin|administrator<br/>
poweruser|power users<br/>
<br>
<b>NOTES:</b><br/>
- For any Drupal role in the mapping, if a user is not mapped to the role, the role will be removed from their profile.
Drupal roles not listed above will not be changed by this module.</br>
- List will be sorted alphabetically on save.
EOD;

    $form['role_mapping'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Mapping of Claim Role Values to Drupal Roles (one per line)'),
      '#default_value' => $this->convertArrayToInputValue($role_mapping),
      '#description' => $this->t($description),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $role_mapping = &$form_state->getValue('role_mapping');
    $role_mapping = $this->convertInputValueToArray($role_mapping);
    asort($role_mapping);
  }


  /**
   * Map Auth0 user role claim values to Drupal roles and add to user.
   *
   * @see openid_connect_auth0_openid_connect_userinfo_save()
   */
  public function mapUserRoles(AccountInterface $account, array $context) {
    $role_key = $this->configuration['claim_to_use_for_role'];
    if (empty($role_key)) {
      return;
    }
    $role_mapping = $this->configuration['role_mapping'];
    if (empty($role_mapping)) {
      return;
    }
    foreach ($role_mapping as $drupal_role) {
      $account->removeRole($drupal_role);
    }

    if (!isset($context['user_data'][$role_key])) {
      return;
    }
    $auth0_role_claim = $context['user_data'][$role_key] ?? [];
    foreach ($auth0_role_claim as $auth0_role) {
      if (!isset($role_mapping[$auth0_role])) {
        continue;
      }
      $account->addRole($role_mapping[$auth0_role]);
    }
  }

  /**
   * Convert a saved string value to an array.
   *
   * @see ListItemBase::extractAllowedValues().
   */
  private function convertInputValueToArray($input_value) {
    $values = [];

    $list = explode("\n", $input_value);
    $list = array_map('trim', $list);
    $list = array_filter($list, 'strlen');

    $generated_keys = $explicit_keys = FALSE;
    foreach ($list as $position => $text) {
      // Check for an explicit key.
      $matches = [];
      if (!preg_match('/(.*)\|(.*)/', $text, $matches)) {
        continue;
      }

      // Trim key and value to avoid unwanted spaces issues.
      $key = trim($matches[1]);
      $value = trim($matches[2]);
      $explicit_keys = TRUE;

      $values[$key] = $value;
    }

    return $values;
  }

  /**
   * @return string
   */
  private function convertArrayToInputValue(array $array) {
    $lines = [];
    foreach ($array as $key => $value) {
      $lines[] = "$key|$value";
    }
    return implode("\n", $lines);
  }
}
